#include <exception>
#include <iostream>

#include "Node.h"
#include "Util.h"


using namespace std;

Node::Node() {
    cStatus = ROOT;
    locked = false;

    pthread_mutex_init(&_mutex, NULL);
    pthread_cond_init(&_cond, NULL);

    pthread_mutex_init(&mutex_precombine, NULL);
    pthread_mutex_init(&mutex_combine, NULL);
    pthread_mutex_init(&mutex_op, NULL);
    pthread_mutex_init(&mutex_distribute, NULL);
}

Node::Node(Node * myParent) {
    parent = myParent;
    cStatus = IDLE;
    locked = false;

    pthread_mutex_init(&_mutex, NULL);
    pthread_cond_init(&_cond, NULL);

    pthread_mutex_init(&mutex_precombine, NULL);
    pthread_mutex_init(&mutex_combine, NULL);
    pthread_mutex_init(&mutex_op, NULL);
    pthread_mutex_init(&mutex_distribute, NULL);
}

Node::~Node() {
}

bool Node::precombine(){
    pthread_mutex_lock(&mutex_precombine);
    while (locked) wait(&_mutex, &_cond);

    switch (cStatus) {
        case IDLE:
            cStatus = FIRST;
            pthread_mutex_unlock(&mutex_precombine);
            return true;
        case FIRST:
            locked = true;
            cStatus = SECOND;
            pthread_mutex_unlock(&mutex_precombine);
            return false;
        case ROOT:
            pthread_mutex_unlock(&mutex_precombine);
            return false;
        default:
            cout << __func__ << " : unexpected node state " << cStatus;
            pthread_mutex_unlock(&mutex_precombine);
            throw exception();
    }
}

int Node::combine(int combined){
    pthread_mutex_lock(&mutex_combine);
    while (locked) wait(&_mutex, &_cond);

    locked = true;
    firstValue = combined;
    switch (cStatus) {
        case FIRST:
            pthread_mutex_unlock(&mutex_combine);
            return firstValue;
        case SECOND:
            pthread_mutex_unlock(&mutex_combine);
            return firstValue + secondValue;
        default:
            cout << __func__ << " : unexpected node state " << cStatus;
            pthread_mutex_unlock(&mutex_combine);
            throw exception();
    }
}

int Node::op(int combined){
    int prior;
    pthread_mutex_lock(&mutex_op);
    switch (cStatus) {
        case ROOT:
            prior = result;
            result += combined;
            pthread_mutex_unlock(&mutex_op);
            return prior;
        case SECOND:
            secondValue = combined;
            locked = false;
            notifyAll(&_mutex, &_cond);
            while (cStatus != RESULT) wait(&_mutex, &_cond);
            locked = false;
            notifyAll(&_mutex, &_cond);
            cStatus = IDLE;
            pthread_mutex_unlock(&mutex_op);
            return result;
       default:
            cout << __func__ << " : unexpected node state " << cStatus;
            pthread_mutex_unlock(&mutex_op);
            throw exception();
    }
}

void Node::distribute(int prior){
    pthread_mutex_lock(&mutex_distribute);
    switch(cStatus) {
        case FIRST:
            cStatus = IDLE;
            locked = false;
            break;
        case SECOND:
            result = prior + firstValue;
            cStatus = RESULT;
            break;
      default:
            cout << __func__ << " : unexpected node state " << cStatus;
            pthread_mutex_unlock(&mutex_distribute);
            throw exception();
    }

    notifyAll(&_mutex, &_cond);
    pthread_mutex_unlock(&mutex_distribute);
}

