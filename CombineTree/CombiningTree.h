#ifndef COMBINING_TREE_H
#define COMBINING_TREE_H

#include "Node.h"


class CombiningTree {

public:
    Node ** _leaf;
    Node ** _nodes;

    unsigned int node_len;

public:
    CombiningTree(int);
    virtual ~CombiningTree();

    int getAndIncrement(int);

};

#endif
