#include <iostream>
#include <stack>
#include "CombiningTree.h"

using namespace std;

CombiningTree::CombiningTree(int width):_leaf(NULL), _nodes(NULL) {
    node_len = width-1;
    int leaf_len = (width+1)/2;

    _nodes = new Node * [node_len];
    _nodes[0] = new Node();

    for(unsigned int i=1; i < node_len; ++i ) {
        _nodes[i] = new Node( _nodes[(i-1)/2] );
    }

    _leaf = new Node *[leaf_len];
    for( int i =0; i < leaf_len; ++i) {
        _leaf[i] = _nodes[node_len -i -1];
    }
}

CombiningTree::~CombiningTree() {
    for(unsigned int i =0; i < node_len; ++i ) {
        if( _nodes[i] != NULL ) {
            delete _nodes[i];
            _nodes[i] = NULL;
        }
    }

    delete [] _nodes;
    delete [] _leaf;
}

int CombiningTree::getAndIncrement(int tid) {
    stack<Node *> stk;
    Node *myLeaf = _leaf[tid/2];

    Node *node = myLeaf;

    while( node->precombine() ) {
        node = node->parent;
    }
    Node *stop = node;

    node = myLeaf;
    int combined = 1;
    while ( node != stop ) {
        combined = node->combine(combined);
        stk.push(node);
        node = node->parent;
    }

    int prior = stop->op(combined);

    while( !stk.empty()) {
        node = stk.top();
        stk.pop();
        node->distribute(prior);
    }

    return prior;

}
