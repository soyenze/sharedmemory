#ifndef NODE_H
#define NODE_H

#include <exception>
#include <pthread.h>

class Node {

private:
    pthread_cond_t _cond;
    pthread_mutex_t _mutex;

    pthread_mutex_t mutex_precombine;
    pthread_mutex_t mutex_combine;
    pthread_mutex_t mutex_op;
    pthread_mutex_t mutex_distribute;

public:
    enum CStatus {
        IDLE = 0,
        FIRST,
        SECOND,
        RESULT,
        ROOT,
        CSTATUS_NUM
    };

    bool locked;
    CStatus cStatus;

    int firstValue, secondValue;
    int result;
    Node * parent;

public:
    Node();
    Node(Node *);
    virtual ~Node();

    bool precombine();
    int combine(int);
    int op(int);
    void distribute(int);

};

#endif
