#ifndef DIFFRACTING_TREE_H
#define DIFFRACTING_TREE_H

#include "DiffractingBalancer.h"

class DiffractingTree {
private:
    DiffractingBalancer * root;
    DiffractingTree ** child;
    int size;
public:
    DiffractingTree(int);
    ~DiffractingTree();
    int traverse(int);
};

#endif
