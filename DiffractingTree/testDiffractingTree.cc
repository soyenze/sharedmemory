#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "DiffractingTree.h"

using namespace std;


const static int MAX_THREAD_NUM = 64;

const static int WORK_TYPE[3] = {10, 50, 100};
const static char WORK_NAME[3][10] = {"High", "Middle", "Low"};

struct threadData {
    long threadID;
    int work;
};


threadData tData[MAX_THREAD_NUM];
DiffractingTree* r = NULL;
pthread_mutex_t mutex_for_log;
int tree_width;

void indexBench(int tid, int work) {
    int input = rand()% (tree_width);
    pthread_mutex_lock(&mutex_for_log);
    int result = r->traverse(tid);
    cout << tid << ":" << input << ", " << result << endl;
    pthread_mutex_unlock(&mutex_for_log);
    usleep((rand()%work)*1000); // the unit of sleep time is microsecond
}

void *threadFunc(void *params)
{
    threadData* tData = reinterpret_cast<threadData *>(params);
    int tid = static_cast<int>(tData->threadID);
    
    indexBench(tid, tData->work);

    pthread_exit(NULL);
}

unsigned int GetTick() {
    timeval tv;
    gettimeofday(&tv, NULL);
    return static_cast<unsigned int>(tv.tv_sec*1000 + tv.tv_usec/1000);
}


int main(int argc, char *argv[])
{
    if( argc != 4 ) {
        cout << "testDiffractingTree [# of Thread] [Tree width] [# of work type(High, Middle, Low)]" << endl;
        exit(0);
    }

    pthread_t threads[MAX_THREAD_NUM];
    pthread_mutex_init(&mutex_for_log, NULL);

    int k;
    int thread_num = atoi(argv[1]);
    tree_width= atoi(argv[2]);
    int work_type = atoi(argv[3]);

    cout << "=== Test Case - Thread num[" << thread_num << "] Tree Width[" << tree_width << "] Contention["<< WORK_NAME[work_type] << "]" << endl; 

    unsigned int startTick = GetTick();

    r = new DiffractingTree(tree_width);

    for(k=0; k < thread_num; ++k) {
        tData[k].threadID = static_cast<long>(k);
        tData[k].work = WORK_TYPE[work_type];

        if(pthread_create(&threads[k], NULL, threadFunc, reinterpret_cast<void*>(&tData[k]))) {
            cout << "[ERROR]: Thread " << k << " during creating thread " << endl;
            exit(0);
        }
    }

    for(k=0; k < thread_num; ++k) {
        if(pthread_join(threads[k], NULL)) {
            cout << "[ERROR]: Thread " << k << " during thread join" << endl;
            exit(0);
        }
    }

    unsigned int endTick = GetTick();

    cout << "=== Total Time : " << endTick - startTick << endl;

    delete r;
    return 0;

}
