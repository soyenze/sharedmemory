#include "Balancer.h"


Balancer::Balancer():toggle(false)  {
    pthread_mutex_init(&_mutex, NULL);
}

Balancer::~Balancer() {
}

bool Balancer::traverse() {

    pthread_mutex_lock(&_mutex);
    bool ret = toggle;
    toggle = !toggle;
    pthread_mutex_unlock(&_mutex);

    return ret;

}
