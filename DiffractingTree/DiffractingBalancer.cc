#include <exception>
#include <iostream>
#include "DiffractingBalancer.h"

using namespace std;

DiffractingBalancer::DiffractingBalancer(int capacity) {
    prism = new Prism(capacity);
    toggle = new Balancer();
}

DiffractingBalancer::~DiffractingBalancer() {
    delete prism;
    delete toggle;
}

int DiffractingBalancer::traverse(int tid) {
#if 1
    try {
        if( prism->visit(tid) ) {
//            cout << "HIT" << endl;
            return 0;
        } else {
//            cout << "HIT" << endl;
            return 1;
        }
    } catch( exception& e ) {
//        cout << "Exception occurs :" << tid << endl;
        return toggle->traverse();
    }
#else
    return toggle->traverse();
#endif
}
