
#include "DiffractingTree.h"

DiffractingTree::DiffractingTree(int mySize):root(NULL), child(NULL), size(mySize) {
    root = new DiffractingBalancer(size);
    if ( size > 2 ) {
        child = new DiffractingTree * [2];
        child[0] = new DiffractingTree(size/2);
        child[1] = new DiffractingTree(size/2);
    }
}

DiffractingTree::~DiffractingTree() {
    delete root;

    if( size > 2 ) {
        for( int i = 0; i < 2; ++i ) {
            delete child[i];
        }
        delete [] child;
    }
}

int DiffractingTree::traverse(int tid) {
    int half = root->traverse(tid);
    if( size > 2)  {
        return (2*(child[half]->traverse(tid)) + half);
    } else {
        return half;
    }
}
