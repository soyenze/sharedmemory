#ifndef PRISM_H
#define PRISM_H

#include <stdlib.h>
#include <time.h>

#include "Exchanger.h"

#define DURATION 5

class Prism {
private:
    Exchanger<int> ** exchanger;
    int capacity;

public:
    Prism(int);
    virtual ~Prism();

    bool visit(int);
};

#endif
