#ifndef DIFFRACTING_BALANCER_H
#define DIFFRACTING_BALANCER_H

#include "Balancer.h"
#include "Prism.h"

class DiffractingBalancer {
private:
    Prism * prism;
    Balancer * toggle;
public:
    DiffractingBalancer(int);
    virtual ~DiffractingBalancer();
    int traverse(int tid);
};

#endif
