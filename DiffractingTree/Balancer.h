#ifndef BALANCER_H
#define BALANCER_H

#include <pthread.h>

class Balancer {

private:
    bool toggle;
    pthread_mutex_t _mutex;
public:
    Balancer();
    virtual ~Balancer();

    bool traverse();

};

#endif
