#ifndef EXCHANGER_H
#define EXCHANGER

#include <errno.h>
#include <pthread.h>
#include <iostream>

using namespace std;


template <typename T>
class Exchanger {
private:
    T* ptr;

    pthread_cond_t _cond_overflow;
    pthread_cond_t _cond;
    pthread_mutex_t _mutex;
    pthread_mutex_t _mutex_for_swap;

    enum { EMPTY, FIRST_ARRIVED, SECOND_ARRIVED } state;

public:
    Exchanger():ptr(NULL), state(EMPTY) {
        pthread_cond_init(&_cond_overflow, NULL);
        pthread_cond_init(&_cond, NULL);
        pthread_mutex_init(&_mutex, NULL);
        pthread_mutex_init(&_mutex_for_swap, NULL);
    }

    virtual ~Exchanger() {
        pthread_cond_init(&_cond_overflow, NULL);
        pthread_cond_init(&_cond, NULL);
        pthread_mutex_init(&_mutex, NULL);
        pthread_mutex_init(&_mutex_for_swap, NULL);

        ptr = 0;
        state = EMPTY;
    }

    int exchange(T *value, const struct timespec *ts) {
        int ret;
        pthread_mutex_lock(&_mutex);

        while( state == SECOND_ARRIVED ) {
            ret = pthread_cond_wait(&_cond_overflow, &_mutex);
            //ret = pthread_cond_timedwait(&_cond_overflow, &_mutex, ts);

            if( ret != 0 ) {
                //cout << "pthread_cond_timedwait error for third thread occurs." << endl;
                pthread_mutex_unlock(&_mutex);
                return ret;
            }
        }

        ret = 0;
        T temp;

        switch(state) {
            case EMPTY:
                ptr = value;
                state = FIRST_ARRIVED;
#if 0
                while( state == FIRST_ARRIVED && ret == 0) {
                    //ret = pthread_cond_wait(&_cond, &_mutex);
                    ret = pthread_cond_timedwait(&_cond, &_mutex, ts);

                    if( ret != 0 ) {
                        //cout << "pthread_cond_timedwait error for first thread occurs." << endl;
                        pthread_mutex_unlock(&_mutex);
                        return ret;
                    }
                }


                pthread_cond_broadcast(&_cond_overflow);

                break;
#else
                ret = pthread_cond_timedwait(&_cond, &_mutex, ts);
                state = EMPTY;
                pthread_cond_broadcast(&_cond_overflow);
                pthread_mutex_unlock(&_mutex);
                return ret;

#endif

            case FIRST_ARRIVED:
                state = SECOND_ARRIVED;

                //cout << "HIT : " << *value << " " << *ptr << endl;

                //pthread_mutex_lock(&_mutex_for_swap);
                temp = *value;
                *value = *ptr;
                *ptr = temp;
                //pthread_mutex_unlock(&_mutex_for_swap);

                pthread_cond_signal(&_cond);

                break;
            default:
                //cout << "pthread_cond_timedwait error for third thread occurs." << endl;
                pthread_mutex_unlock(&_mutex);
                return EINVAL;

        }

        pthread_mutex_unlock(&_mutex);

        return 0;
    }

};

#endif
