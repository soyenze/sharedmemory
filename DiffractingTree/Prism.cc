#include <exception>
#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>

#include "Prism.h"

using namespace std;

Prism::Prism(int _capacity):exchanger(NULL), capacity(_capacity) {
    srand(time(NULL));

    exchanger = new Exchanger<int>* [capacity];

    for( int i=0; i < capacity; ++i ) {
        exchanger[i] = new Exchanger<int>();
    }
}

Prism::~Prism() {
    for( int i = 0; i < capacity; ++i ) {
        delete exchanger[i];
    }

    delete [] exchanger;
}

bool Prism::visit(int tid) {

    struct timespec ts;
    struct timeval tp;

    gettimeofday(&tp, NULL);
    ts.tv_sec = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec + (DURATION *1000);

    int me = tid;
    int other = me;
    int slot = rand()%capacity;
    int ret = exchanger[slot]->exchange(&other, &ts);

    //cout << "HIT : " << me << " " << other << endl;

    if(ret == 0)    return (tid < other);
    else            throw exception();
    //return (tid < other);
}
