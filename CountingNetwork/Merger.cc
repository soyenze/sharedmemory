#include "Merger.h"

Merger::Merger(int myWidth):width(myWidth) {
    layer = new Balancer * [width/2];

    for(int i =0; i < width/2; ++i) {
        layer[i] = new Balancer();
    }

    if (width > 2) {
        half = new Merger * [2];
        half[0] = new Merger(width/2);
        half[1] = new Merger(width/2);
    }
}

Merger::~Merger() {
    for(int i =0; i<width/2; ++i ) {
        delete layer[i];
    }

    if( width > 2) {
        for( int i =0; i < 2; ++i) {
            delete half[i];
        }
        delete [] half;
    }
    
    delete [] layer;
}

int Merger::traverse(int input) {
    int output = 0;
    if( width > 2 ) {
        if(input < width / 2) {
            output = half[input %2]->traverse(input/2);
        } else {
            output = half[ 1-(input%2) ]->traverse(input/2);
        }
    }

    return (2*output) + layer[output]->traverse();
}
