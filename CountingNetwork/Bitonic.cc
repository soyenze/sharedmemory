#include "Bitonic.h"
#include <iostream>

using namespace std;

Bitonic::Bitonic(int myWidth):width(myWidth) {
    merger = new Merger(width);
    if( width > 2 ) {
        half = new Bitonic * [2];
        half[0] = new Bitonic(width/2);
        half[1] = new Bitonic(width/2);
    }
}

Bitonic::~Bitonic() {
    delete merger;

    if( width > 2 ) {
        for( int i = 0; i < 2; ++i ) {
            delete half[i];
        }

        delete [] half;
    }
}

int Bitonic::traverse(int input)  {
    int output = 0;
    if( width>2 ) {
        output = half[input/(width/2)]->traverse(input/2);
    }
    return merger->traverse( (input>= (width/2) ? (width/2):0) + output);
}

