#ifndef BITONIC_H
#define BITONIC_H

#include "Merger.h"

class Bitonic {
private:
    Bitonic ** half;
    Merger * merger;
    int width;
public:
    Bitonic(int);
    virtual ~Bitonic();

    int traverse(int);
};

#endif
