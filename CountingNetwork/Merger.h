#ifndef MERGER_H
#define MERGER_H

#include "Balancer.h"

class Merger {
private:
    Merger ** half;
    Balancer **layer;
    int width;
public:
    Merger(int);
    virtual ~Merger();

    int traverse(int);

};

#endif
